## How to Run The Project:

* Change the appropriate settings in .env file.
* Run the command `yarn run start` or `npm run start`

**Note: If you change the host or the port, make sure to make the required changes in the client as well.**

**Note: And also note that this project cannot be deployed to the cloud and must be run locally because it makes use of the file system for archiving files. Also, the WinRaR command line tool must be in the PATH.**