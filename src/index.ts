require('dotenv').config();
import express from 'express';
import morgan from 'morgan';
import { ApolloServer } from 'apollo-server-express';
import cors from 'cors';

import { connect } from './database/connection';
import { resolvers } from './graphql/resolvers';
import { typeDefs } from './graphql/typeDefs';
import { Database } from './database/types';

async function init() {
	const app = express();
	app.use(morgan('combined'));
	app.use(cors());
	app.use(express.static('static'));

	const db: Database = await connect();

	const apolloServer = new ApolloServer({
		resolvers,
		typeDefs,
		context: ({ req, res }) => ({ req, res, db }),
	});

	apolloServer.applyMiddleware({ app, path: '/api' });

	app.get('/*', (req, res) => {
		res.sendFile('/static/index.html');
	});

	const server = app.listen(process.env.PORT, () => {
		console.log(
			`[app]: server started successfully on port ${process.env.PORT}`
		);
	});

	server.timeout = 0;
}

init();
