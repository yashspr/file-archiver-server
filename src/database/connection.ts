import { MongoClient } from 'mongodb';
import { Database } from './types';

export const connect = async (): Promise<Database> => {
	const url = 'mongodb://localhost:27017/file-archiver';
	const dbName = 'file-archiver';

	const client = new MongoClient(url, {
		useNewUrlParser: true,
		useUnifiedTopology: true,
	});

	try {
		await client.connect();
		console.log('[database]: connected to database successfully');

		const db = client.db(dbName);

		return {
			files: db.collection('files'),
			folders: db.collection('folders'),
			archives: db.collection('archives'),
		};
	} catch (err) {
		console.error('[database]: error connecting to database: ' + err);
		throw err;
	}
};
