import { ObjectId, Collection } from 'mongodb';

export interface File {
	_id: ObjectId;
	archiveName: string;
	ext: string;
	name: string;
	path: string;
	size: number;
	parentId: ObjectId | null;
}

export enum FileType {
	FILE = 'file',
	FOLDER = 'folder',
}

export interface Child {
	id: ObjectId;
	type: FileType;
}

export interface Folder {
	_id: ObjectId;
	archiveName: string;
	name: string;
	path: string;
	parentId: ObjectId | null;
	children?: Child[];
}

export interface Archive {
	_id: ObjectId;
	name: string;
	children: Child[];
}

export interface Database {
	files: Collection<File>;
	folders: Collection<Folder>;
	archives: Collection<Archive>;
}
