import { ObjectId } from 'mongodb';
import { FileType } from '../../database/types';

export interface ObjectIdMapping {
	[filename: string]: {
		id: ObjectId;
		type: FileType;
	};
}
