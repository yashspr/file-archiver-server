import fs from 'fs';
import path from 'path';
import { spawn } from 'child_process';
import uid from 'uid';

export async function getContents(dir: string) {
	try {
		let contents = await fs.promises.readdir(dir);
		return contents;
	} catch (err) {
		console.log(err);
		throw err;
	}
}

export async function getSize(filepath: string): Promise<number> {
	let stats = await fs.promises.stat(filepath);
	if (stats.isFile()) {
		return stats.size;
	}
	if (stats.isDirectory()) {
		let size = 0;
		let contents = await fs.promises.readdir(filepath);
		for (const filename of contents) {
			size += await getSize(path.join(filepath, filename));
		}
		return size;
	}
	return 0;
}

export async function isFile(filepath: string) {
	try {
		let stats = await fs.promises.stat(filepath);
		if (stats.isFile()) {
			return true;
		}
		return false;
	} catch (err) {
		console.log(err);
		throw err;
	}
}

export async function isFolder(filepath: string) {
	try {
		let stats = await fs.promises.stat(filepath);
		if (stats.isDirectory()) {
			return true;
		}
		return false;
	} catch (err) {
		console.log(err);
		throw err;
	}
}

export function cleanName(name: string) {
	name = name.replace(/[']/g, '');
	name = name.replace(/[.,_&)(\[\]-]/g, ' ');
	return name.replace(/[ ]+/g, ' ');
}
