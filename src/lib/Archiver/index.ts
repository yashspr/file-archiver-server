import fs from 'fs';
import path from 'path';
import { spawn } from 'child_process';
import uid from 'uid';
import { getContents, isFolder, isFile, getSize, cleanName } from './utils';
import { ObjectId } from 'mongodb';
import { Database, FileType } from '../../database/types';
import { ObjectIdMapping } from './types';

async function moveOneLevelUp(filepath: string) {
	try {
		let contents = await getContents(filepath);
		if (contents.length > 1) {
			return false;
		}

		let oldPath = path.join(filepath, contents[0]);
		let newPath = path.join(filepath, '../', contents[0]);
		await fs.promises.rename(oldPath, newPath);
		// you must also remove the original directory which is empty
		await fs.promises.rmdir(filepath);
		return true;
	} catch (err) {
		console.log(err);
		throw err;
	}
}

async function isDirEmpty(filepath: string) {
	try {
		let contents = await getContents(filepath);
		if (contents.length === 0) {
			return true;
		} else {
			return false;
		}
	} catch (err) {
		console.error(err);
		throw err;
	}
}

async function normalizeContents(rootPath: string) {
	let contents = await getContents(rootPath);
	for (const filename of contents) {
		if (await isFolder(path.join(rootPath, filename))) {
			let folderPath = path.join(rootPath, filename);
			await normalizeContents(folderPath);
			if (await isDirEmpty(folderPath)) {
				await fs.promises.rmdir(folderPath);
			} else {
				await moveOneLevelUp(folderPath);
			}
		}
	}
}

async function storeFileStructureInDb(
	rootPath: string,
	absolutePath: string = '',
	parentId: ObjectId | null = null,
	archiveName: string,
	db: Database
): Promise<ObjectIdMapping> {
	let contents = await getContents(rootPath);
	let objectIdMapping: ObjectIdMapping = {};
	for (let filename of contents) {
		try {
			if (await isFile(path.join(rootPath, filename))) {
				let stats = await fs.promises.stat(path.join(rootPath, filename));
				// TODO: clean file name by removing unnecessary punctuation
				let insertResult = await db.files.insertOne({
					name: cleanName(filename),
					path: path.join(absolutePath, filename),
					ext: path.extname(filename),
					archiveName,
					size: stats.size,
					parentId: parentId ? parentId : null,
				});
				let id = insertResult.ops[0]._id;
				objectIdMapping[filename] = {
					id,
					type: FileType.FILE,
				};
			}

			if (await isFolder(path.join(rootPath, filename))) {
				// TODO: clean file name by removing unnecessary punctuation
				let insertResult = await db.folders.insertOne({
					name: cleanName(filename),
					archiveName,
					path: path.join(absolutePath, filename),
					parentId: parentId ? parentId : null,
				});

				let id = insertResult.ops[0]._id;
				let childObjectIdMappings: ObjectIdMapping = await storeFileStructureInDb(
					path.join(rootPath, filename),
					path.join(absolutePath, filename),
					id,
					archiveName,
					db
				);

				await db.folders.updateOne(
					{ _id: id },
					{
						$set: {
							children: Object.values(childObjectIdMappings),
						},
					}
				);

				objectIdMapping[filename] = {
					id,
					type: FileType.FOLDER,
				};
			}
		} catch (err) {
			console.log(err);
			// If error occurs, it must be halted and rolled back
			throw err;
		}
	}

	return objectIdMapping;
}

async function folderPartition(rootPath: string) {
	let contents = await getContents(rootPath);

	let fileSizes = [];
	for (let filename of contents) {
		fileSizes.push([filename, await getSize(path.join(rootPath, filename))]);
	}

	fileSizes.sort((a, b) => {
		return (a[1] as number) - (b[1] as number);
	});

	let archiveName = uid(24);
	let currentArchiveSize = 0;

	await fs.promises.mkdir(path.join(rootPath, archiveName));

	while (fileSizes.length > 0) {
		let currentFile = fileSizes.shift();
		if (currentFile[1] + currentArchiveSize <= process.env.MAX_ARCHIVE_SIZE) {
			currentArchiveSize += currentFile[1];
		} else {
			currentArchiveSize = currentFile[1];
			archiveName = uid(24);
			await fs.promises.mkdir(path.join(rootPath, archiveName));
		}

		let oldPath = path.join(rootPath, currentFile[0]);
		let newPath = path.join(rootPath, archiveName, currentFile[0]);
		await fs.promises.rename(oldPath, newPath);
	}
}

export async function archiver(rootPath: string, db: Database): Promise<void> {
	rootPath = rootPath.replace(/\\/g, '\\\\');
	rootPath = path.normalize(rootPath);

	await normalizeContents(rootPath);
	console.log('normalized contents successfully');

	await folderPartition(rootPath);
	console.log('partitioned files successfully');

	let contents = await getContents(rootPath);

	for (let archive of contents) {
		let archiveName = archive + '.rar';

		let currentArchiveContents = await getContents(
			path.join(rootPath, archive)
		);

		let args = ['a', archiveName];

		for (let file of currentArchiveContents) {
			args.push(file);
		}

		let rarInstance = spawn('C:\\Program Files\\WinRAR\\Rar.exe', args, {
			cwd: path.join(rootPath, archive),
		});

		await new Promise((resolve, reject) => {
			rarInstance.on('exit', (code, signal) => {
				// console.log(code, signal);
				// console.log('Archive created successfully');
				if (code == 0) {
					resolve();
				} else {
					reject('Error code: ' + code);
				}
			});

			rarInstance.on('error', (err) => {
				console.log('Error occurred when creating archive: ' + err);
				reject(err);
			});

			rarInstance.stdout.pipe(process.stdout);
		});

		await fs.promises.rename(
			path.join(rootPath, archive, archiveName),
			path.join(process.env.ARCHIVES_ROOT_PATH, archiveName)
		);

		let objectIdMappings = await storeFileStructureInDb(
			path.join(rootPath, archive),
			'',
			null,
			archiveName,
			db
		);
		console.log(
			'inserted all files and folders in the archive ' +
				archiveName +
				' to the db successfully'
		);

		await db.archives.insertOne({
			name: archiveName,
			children: Object.values(objectIdMappings),
		});
		console.log('inserted archive data to the db successfully');
	}
}
