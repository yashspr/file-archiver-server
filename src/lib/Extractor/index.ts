import path from 'path';
import fs from 'fs';
import { spawn, exec } from 'child_process';
import { File, FileType } from '../../database/types';
import uid from 'uid';

export async function extract(
	archiveName: string,
	pathInArchive: string,
	fileType: FileType
) {
	let workingDirPath = path.join(process.cwd(), 'static/tmp/');
	console.log(workingDirPath);
	const { base, name, ext } = path.parse(pathInArchive);

	if (fileType == FileType.FILE) {
		// possbile bug here
		// There is a chance that the same file will be extracted simultaneously into the same directory.
		// Solution is to created a temporary directory before extraction.

		console.log(path.join(process.env.ARCHIVES_ROOT_PATH, archiveName));
		console.log(pathInArchive);

		let command = `"C:\\Program Files\\WinRAR\\Rar.exe" e "${path.join(
			process.env.ARCHIVES_ROOT_PATH,
			archiveName
		)}" "${pathInArchive}" "${workingDirPath}"`;
		console.log(command);

		let rarInstance = exec(command);

		try {
			await new Promise((resolve, reject) => {
				rarInstance.on('exit', (code, signal) => {
					// console.log(code, signal);
					// console.log('File extracted successfully');
					if (code == 0) {
						resolve();
					} else {
						reject('Error code: ' + code);
					}
				});

				rarInstance.on('error', (err) => {
					console.log('Error occurred when extracting: ' + err);
					reject(err);
				});

				rarInstance.stdout.pipe(process.stdout);
			});

			let randomString = uid(18);
			let newFileName = `${name}-${randomString}${ext}`;

			await fs.promises.rename(
				path.join(workingDirPath, base),
				path.join(workingDirPath, newFileName)
			);

			return path.join(workingDirPath, newFileName);
		} catch (err) {
			console.log(err);
			throw err;
		}
	}

	if (fileType == FileType.FOLDER) {
		let randomString = uid(12);
		let tmpDirName = `${base}-${randomString}\\`;
		let tmpDirPath = path.join(workingDirPath, tmpDirName);

		console.log(tmpDirName);
		console.log(tmpDirPath);

		await fs.promises.mkdir(tmpDirPath);

		let args = [
			'x',
			'-ep1',
			path.join(process.env.ARCHIVES_ROOT_PATH, archiveName),
			pathInArchive,
			tmpDirPath,
		];

		try {
			let rarInstance = spawn('C:\\Program Files\\WinRAR\\Rar.exe', args);

			await new Promise((resolve, reject) => {
				rarInstance.on('exit', (code, signal) => {
					// console.log(code, signal);
					// console.log('File extracted successfully');
					if (code == 0) {
						resolve();
					} else {
						reject('Error code: ' + code);
					}
				});

				rarInstance.on('error', (err) => {
					console.log('Error occurred when extracting: ' + err);
					reject(err);
				});

				rarInstance.stdout.pipe(process.stdout);
			});

			let archiveName = `${base}-${uid(12)}.rar`;
			console.log(path.join(tmpDirPath, base));
			args = ['a', '-ep1', archiveName, path.join(tmpDirPath, base)];

			rarInstance = spawn('C:\\Program Files\\WinRAR\\Rar.exe', args, {
				cwd: workingDirPath,
			});

			await new Promise((resolve, reject) => {
				rarInstance.on('exit', (code, signal) => {
					// /console.log(code, signal);
					// console.log('Archive created successfully');
					if (code == 0) {
						resolve();
					} else {
						reject('Error code: ' + code);
					}
				});

				rarInstance.on('error', (err) => {
					console.log('Error occurred when creating archive: ' + err);
					reject(err);
				});

				rarInstance.stdout.pipe(process.stdout);
			});

			await fs.promises.rmdir(tmpDirPath, {
				recursive: true,
			});

			return path.join(workingDirPath, archiveName);
		} catch (err) {
			console.log(err);
			throw err;
		}
	}
}
