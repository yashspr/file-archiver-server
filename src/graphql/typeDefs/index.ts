import { gql } from 'apollo-server-express';

export const typeDefs = gql`
	enum FileType {
		file
		folder
	}

	enum QueryType {
		files
		folders
		all
	}

	enum ArchiverStatus {
		ALL_OK
		FAIL
	}

	type SearchResult {
		id: ID!
		name: String!
		type: FileType!
	}

	type ArchiverResult {
		status: ArchiverStatus!
	}

	type ExtractorResult {
		downloadLink: String!
	}

	type Query {
		search(query: String!, type: QueryType): [SearchResult!]!
		extract(id: ID!, type: FileType): ExtractorResult!
	}

	type Mutation {
		archiver(rootPath: String!): ArchiverResult
	}
`;
