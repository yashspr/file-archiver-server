import { IResolvers } from 'apollo-server-express';
import { SearchArgs, SearchResult } from './types';
import { Database } from '../../../database/types';
import { FileType } from './types';

const searchResolver: IResolvers = {
	Query: {
		search: async (
			_root: undefined,
			{ query, type }: SearchArgs,
			{ db }: { db: Database }
		): Promise<SearchResult[]> => {
			try {
				let results: SearchResult[] = [];

				console.log(query);

				if (type == 'files' || type == 'all') {
					let filesCursor = await db.files.find(
						{
							$text: {
								$search: query,
							},
						},
						{
							projection: {
								score: {
									$meta: 'textScore',
								},
							},
							sort: {
								score: {
									$meta: 'textScore',
								},
							},
						}
					);
					let documents = await filesCursor.toArray();
					let files: SearchResult[] = documents.map((document) => ({
						id: document._id,
						name: document.name,
						type: FileType.FILE,
					}));

					results = results.concat(files);
				}

				if (type == 'folders' || type == 'all') {
					let foldersCursor = await db.folders.find({
						$text: {
							$search: query,
						},
					});
					let documents = await foldersCursor.toArray();
					let folders: SearchResult[] = documents.map((document) => ({
						id: document._id,
						name: document.name,
						type: FileType.FOLDER,
					}));

					results = results.concat(folders);
				}

				return results;
			} catch (err) {
				console.error(err);
				throw err;
			}
		},
	},
};

export default searchResolver;
