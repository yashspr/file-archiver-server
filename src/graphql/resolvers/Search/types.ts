import { ObjectId } from 'mongodb';

export enum FileType {
	FILE = 'file',
	FOLDER = 'folder',
}

enum QueryType {
	FILES = 'files',
	FOLDERS = 'folders',
	ALL = 'all',
}

export interface SearchArgs {
	query: string;
	type: QueryType;
}

export interface SearchResult {
	id: ObjectId;
	name: string;
	type: FileType;
}
