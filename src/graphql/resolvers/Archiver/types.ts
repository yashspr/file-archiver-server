export interface ArchiverArgs {
	rootPath: string;
}

export enum ArchiverStatus {
	ALL_OK = 'ALL_OK',
	FAIL = 'FAIL',
}

export interface ArchiverResult {
	status: ArchiverStatus;
}
