import { IResolvers } from 'apollo-server-express';
import { ArchiverArgs, ArchiverResult, ArchiverStatus } from './types';
import { Database } from '../../../database/types';
import { archiver } from '../../../lib/Archiver';

const archiverResolver: IResolvers = {
	Mutation: {
		archiver: async (
			_root: undefined,
			{ rootPath }: ArchiverArgs,
			{ db }: { db: Database }
		): Promise<ArchiverResult> => {
			if (rootPath.length > 0) {
				console.log('archiver mutation called');
				try {
					await archiver(rootPath, db);
					return {
						status: ArchiverStatus.ALL_OK,
					};
				} catch (err) {
					console.log(err);
					return {
						status: ArchiverStatus.FAIL,
					};
				}
			} else {
				return {
					status: ArchiverStatus.FAIL,
				};
			}
		},
	},
};

export default archiverResolver;
