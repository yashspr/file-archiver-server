import path from 'path';
import { IResolvers } from 'apollo-server-express';
import { ExtractorArgs, ExtractorResult } from './types';
import { Database, FileType, File, Folder } from '../../../database/types';
import { extract } from '../../../lib/Extractor';
import { ObjectId } from 'mongodb';

const extractorResolver: IResolvers = {
	Query: {
		extract: async (
			_root: undefined,
			{ id, type }: ExtractorArgs,
			{ db }: { db: Database }
		): Promise<ExtractorResult> => {
			try {
				let extractedFilePath: string;

				if (type == FileType.FILE) {
					let file: File = await db.files.findOne({ _id: new ObjectId(id) });
					extractedFilePath = await extract(file.archiveName, file.path, type);
				} else if (type == FileType.FOLDER) {
					let folder: Folder = await db.folders.findOne({
						_id: new ObjectId(id),
					});
					extractedFilePath = await extract(
						folder.archiveName,
						folder.path,
						type
					);
				}

				const { base } = path.parse(extractedFilePath);
				let downloadLink = `http://${process.env.HOST}/tmp/${base}`;

				return {
					downloadLink,
				};
			} catch (err) {
				console.error(err);
				throw err;
			}
		},
	},
};

export default extractorResolver;
