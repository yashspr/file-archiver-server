import { ObjectId } from 'mongodb';
import { FileType } from '../../../database/types';

export interface ExtractorArgs {
	id: ObjectId;
	type: FileType;
}

export interface ExtractorResult {
	downloadLink: string;
}
