import merge from 'lodash.merge';

import searchResolver from './Search';
import archiverResolver from './Archiver';
import extractorResolver from './Extractor';

export const resolvers = merge(
	searchResolver,
	archiverResolver,
	extractorResolver
);
