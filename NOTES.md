TODO FEATURES:

[ ] Add pagination when returning results
[ ] Store the hash in the db and use it to remove duplicates
[ ] Ability to perform deep search which uses regular expression rather than indexes
[ ] Perform a precheck for permissions before attempting anything and if errors are present, show in the browser
[ ] supprot for depth when moving single files one level up